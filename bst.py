from node import Node

class BST():
    def __init__(self, value: int):
        self.root = Node(value)
        return

    def add_child(self, value: int):
        if self.root is None:
            self.root = Node(value)
        else:
            if self.root.value < value:
                if self.root.right_child is None:
                    self.root.right_child = BST(value)
                else:
                    self.root.right_child.add_child(value)
            else:
                if self.root.left_child is None:
                    self.root.left_child = BST(value)
                else:
                    self.root.left_child.add_child(value)
        return

    def print_inorder(self):
        if self.root:
            if not self.root.left_child is None:
                self.root.left_child.print_inorder()
            print(self.root.value)
            if not self.root.right_child is None:
                self.root.right_child.print_inorder()
        return

