from node import Node
from bst import BST

def add_child(root: Node, value: int):
    if root is None:
        root = Node(value)
    else:
        if root.value < value:
            if root.right_child is None:
                root.right_child = Node(value)
            else:
                add_child(root.right_child, value)
        else:
            if root.left_child is None:
                root.left_child = Node(value)
            else:
                add_child(root.left_child, value)
    return

def print_inorder(root: Node):
    if root:
        print_inorder(root.left_child)
        print(root.value)
        print_inorder(root.right_child)
    return

def main():
    root = Node(50)
    add_child(root, 30)
    add_child(root, 20)
    add_child(root, 10)
    add_child(root, 60)
    add_child(root, 80)
    add_child(root, 5)
    add_child(root, 100)
    add_child(root, 60)
    add_child(root, 70)
    print_inorder(root)

    bst = BST(50)
    bst.add_child(10)
    bst.add_child(20)
    bst.add_child(60)
    bst.add_child(70)
    bst.add_child(80)
    bst.add_child(20)
    bst.add_child(40)
    bst.add_child(10)
    bst.add_child(60)
    bst.add_child(70)
    bst.add_child(15)
    bst.print_inorder()
    return

if __name__ == "__main__":
    main()

